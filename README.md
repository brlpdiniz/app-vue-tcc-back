# Manual de Funcionamento - Back-end

| GRR | NOME |
| ------ | ------ |
| 2017208552 | Bruno Leandro Diniz |
| 2021161408 | João Vitor Ribeiro de Moraes |
| 2020245236 | Levi Passos do Pinho |
| 2020132435 | José Kozechen Farias |
<br>

## Instalar as dependências do projeto:
<br>

1. Atualização e instalação pacotes essenciais:
	```
	apt update && apt install -y unzip git vim
	```
    
2. Instalação pacotes adicionais necessários:
    ```
    apt install -y lsb-release ca-certificates apt-transport-https software-properties-common wget curl
    ```
    
3. Configuração do ambiente Node.js:
    ```
    curl -s https://deb.nodesource.com/setup_16.x | bash
    ```
    
4. Atualização dos pacotes do sistema:
    ```
    apt update -y
    ```
    
5. Instalação do Node.js:
    ```
    apt install -y nodejs
    ```
    
6. Atualização do npm para a versão 9.4.1:
    ```
    npm install -g npm@9.4.1
    ```
    
7. Instalação da biblioteca libsodium-dev:
    ```
    apt install -y libsodium-dev
    ```
    
8. Configuração do repositório PHP 8.0 - Parte 1:
	```
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
    ```
    
9. Configuração do repositório PHP 8.0 - Parte 2:
    ```
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" |  tee /etc/apt/sources.list.d/php.list
    ```

10. Configuração do repositório Sury PHP:
    ```
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" |  tee /etc/apt/sources.list.d/sury-php.list
    ```

11. Adição da chave do repositório Sury PHP:
    ```
    curl -fsSL  https://packages.sury.org/php/apt.gpg| gpg --dearmor -o /etc/apt/trusted.gpg.d/sury-keyring.gpg
    ```

12. Atualização da lista de pacotes:
    ```
    apt update
    ```

13. Instalação dos pacotes PHP 8.2 necessários:
    ```
    apt install -y php8.2-fpm php8.2-cli php8.2-imap php8.2-mysql php8.2-curl php8.2-gd php8.2-redis php8.2-xml php8.2-zip`
    ```

14. Instalação do Composer:
    ```
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    ```

15. Instalação do MariaDB Server e Client:
    ```
    apt-get update -y && apt install -y mariadb-server mariadb-client
    ```
<br>

## Comandos para o funcionamento do Back-end
<br>

16. Clonar o repositorio do GIT (Back-end):
    ```
    git clone https://gitlab.com/brlpdiniz/app-vue-tcc-back
    ```
    
17. Iniciar MariaDB/Banco de dados/Mysql:
    ```
	service mariadb start
    ```

18. Instalação dos pacotes do projeto:
    ```
    compose install
    ```
    
20. Iniciar servidor back-end: 
    ```
    php artisan serve --port=8001`
    ```