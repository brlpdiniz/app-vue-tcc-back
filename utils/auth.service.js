const login = async (user) => {
    try {
        const body = {};
        const response = await useFetch(`http://localhost:8001/api/login`, {
            method: "POST",
            body: user,
        });

        if (response?.data?.value?.succes) {
            let loginResponse = response.data.value;
            return loginResponse;
        } else {
            throw new Error("Falha ao fazer login!");
        }
    } catch (error) {
        throw new Error(error.message);
    }
};

const register = async (user) => {
    try {
        const response = await useFetch(`http://localhost:8001/api/register`, {
            method: "POST",
            body: user,
        });
        if (response.data.value.success) {
            return response.data.value;
        } else {
            throw new Error("Falha ao cadastrar usuário!");
        }
    } catch (error) {
        console.error("Erro ao fazer o registro:", error.message);
        throw new Error("Falha ao conectar ao servidor");
    }
};

export default {
    login,
    register
};
